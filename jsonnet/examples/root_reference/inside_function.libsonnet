local isFunc(data) =
  data { config: { a: 'fromFunc' } };


local base = ({ c: { d: $.config.a } } +
              { config: { a: 'fromBase' } });

local isFuncNested(data) =
  data.c { config: { a: 'fromNested' } };

// isFunc(base) + { config: { a: 'z' } }

// { func: isFunc(base) } + { config: { a: 'z' } }

// { func: isFunc(base) } + { func+: { config: { a: 'z' } } }

// isFuncNested(base)

// { y: isFuncNested(base) }

// { y: isFuncNested(base) } + { config: { a: 'yy' } }

{ c: isFuncNested(base) } + { config: { a: 'fromMerge' } }
