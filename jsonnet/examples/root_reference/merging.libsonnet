local func(data) =
  data { config: { a: 'fromFunc' } };


local base = ({ c: { d: $.config.a } } +
              { config: { a: 'fromBase' } });

local funcNested(config) =
  base + config;

local config = { config: { a: 'fromNested' } };

// isFunc(base) + { config: { a: 'z' } }

// { func: isFunc(base) } + { config: { a: 'z' } }

// { func: isFunc(base) } + { func+: { config: { a: 'z' } } }

// isFuncNested(base)

// { y: isFuncNested(base) }

// { y: isFuncNested(base) } + { config: { a: 'yy' } }


{
  config: { a: 'fromStart' },
  target: { c: base.c },
} +
{
  local basePatch = base + config,
  config: { a: 'fromEnd' },
  target+: { config: $.config },
}
