{
  hidden:: { you: 'do not see me' },
} + {
  clear: $.hidden,
}
