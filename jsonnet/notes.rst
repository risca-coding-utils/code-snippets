
https://stackoverflow.com/questions/64447270/jsonnet-conditional-generation-of-a-field
https://stackoverflow.com/questions/54886373/overwrite-a-nested-list-element-using-jsonnet

=========
Libraries
=========

:jsonnet-modifiers: Modify nested key and more, `see <https://github.com/sbarzowski/jsonnet-modifiers/blob/master/README.md>`_

For k8s:
- https://github.com/Unity-Technologies/jsonnet-libs/blob/master/util/k8s.libsonnet
- `grafana collections of various services <https://github.com/grafana/jsonnet-libs>`_

`Search for more <https://github.com/search?q=jsonnet+lib&type=repositories&p=2>`_ on github.

========
Examples
========

K8s `configmap with suffix <https://github.com/grafana/tanka/discussions/727>`_.
