#!/usr/bin/env bash
# Small on request sshd daemon
#
# Docs: https://rgoswami.me/posts/tinyssh-dockerdev-env/

INSTALL="${INSTALL:-sudo apt install -y}"

${INSTALL} tinysshd uscpi-tcp

mkdir $HOME/.ssh
touch $HOME/.ssh/authorized_keys
chmod 700 .ssh/
echo $COPIED_KEY >> $HOME/.ssh/authorized_keys
chmod 600 .ssh/authorized_keys
tinysshd-makekey $HOME/tinysshkeydir
tcpserver -HRDl0 0.0.0.0 2200 /usr/sbin/tinysshd -v $HOME/tinysshkeydir


# For connecting:
# docker inspect -f "{{ .NetworkSettings.IPAddress }}" $IMG_NAME
# ssh $USER@$IP -p 2200



# On the host:
# # On the host
# ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519_tiny
# ssh-add ~/.ssh/id_ed25519_tiny
# # Copy the public key
# cat ~/.ssh/id_ed25519_tiny.pub | wl-copy # wayland

