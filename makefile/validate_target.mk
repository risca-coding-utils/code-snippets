
_ALLOW_NO_ENV = help exportall vendor is_updated is_updated.jb is_updated.tk·
ifeq ($(ENV),undefined)
  ifeq ($(MAKECMDGOALS),)
  else ifneq ($(filter $(MAKECMDGOALS),$(_ALLOW_NO_ENV)),)
  else
    $(error "Please set a proper environment variable (ENV=<MYENV>). See ENV variable description.")
  endif
endif

